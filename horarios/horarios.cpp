#include <iostream>
#include <cstdio>
#include <vector>
#include <cstdio>
#include <sstream>
#include <map>
#include <fstream>
using namespace std;
typedef pair< int, int > hlibre;

#define ini first
#define fin second
#define WSIZE 7 //size of week

vector<hlibre> gfree[WSIZE];//general free schedule

int bsearch(vector<hlibre> &v,int f, int b, int e){
	int pos= (b+e)>>1;
	if (f==v[pos].ini) return pos;
	else if(b==e) return b-1;
	else{
		if (f>v[pos].ini)return bsearch(v, f, pos+1, e);
		return bsearch(v, f, b, pos);
	}
}

string dias="montuewedthufrisatsun";
inline int h2i(int h, int m ){return h*60 + m;}//hour to int function
inline hlibre i2h(int n){return hlibre(n/60, n-(60 * (n / 60)));}//int to hour function
inline int getday(string day){return dias.find(day)/3;};

class hpeople{
public:
	vector<hlibre> free[WSIZE];
	hpeople(string filename){
		ifstream in(filename.c_str());
		string line="", dia;		
		int hi,mi, hf, mf, nday;
		char horalibre[15];	
		while(getline(in,line), line!=""){
			stringstream ss(line);
			ss>>dia;
			nday = getday(dia);
			while(ss>>horalibre){
				sscanf(horalibre, "%d:%d-%d:%d", &hi, &mi, &hf, &mf);				
				hlibre h(h2i(hi, mi), h2i(hf, mf));
				free[nday].push_back(h);
			}
			line="";
		}
	}
	inline int fdays(){
		int n=0;for(int i=0; i < WSIZE; ++i)
		if (free[i].size()) n++;return n;
	}	


	void p(){
		for(int i=0; i< WSIZE; ++i){		
			if (!free[i].size()) continue;
			cout<<dias.substr(i*3, 3)<<" ";
			for(int j = 0; j < free[i].size(); ++j)
				cout<<free[i][j].ini<<" "<<free[i][j].fin<<"\t";
			cout<<endl;
		}
	}

};

void p(vector<hlibre> *free, int minimun_time){
	bool pday=true;
	for(int i=0; i< WSIZE; ++i){		
		if (!free[i].size()) continue;
		pday=true;
		for(int j = 0; j < free[i].size(); ++j){
			if (minimun_time > free[i][j].fin - free[i][j].ini) continue;
			if (pday)	{
				cout<<dias.substr(i*3, 3)<<" ";
				pday=false;
			}
			hlibre hi=i2h(free[i][j].ini), hf = i2h(free[i][j].fin);
			cout<<hi.ini<<":"<<hi.fin<<"-" << hf.ini<<":"<<hf.fin <<"\t";
			//cout<<free[i][j].ini<<" "<<free[i][j].fin<<"\t";
		}
		cout<<endl;
	}
}

void copy(vector<hlibre> &from, vector<hlibre> &to){
	to.clear();
	to.resize(from.size());
	for(int i=0; i< from.size(); ++i)to[i]=from[i];
}
vector<hlibre> merge_free(vector<hlibre> *a, vector<hlibre> *b){				
	if ((*a)[0].ini > (*b)[0].ini)swap(a, b);
	vector<hlibre> intersections;
	int i = 0, j = 0, intersec;
	while(i < a->size() || j < b->size()){
		i=intersec=bsearch(*a,(*b)[j].ini, 0, a->size());
		//cout<<"i:"<<i<<endl;
		if ((*b)[j].ini < (*a)[i].fin){//intersecan
			if ((*b)[j].fin<=(*a)[i].fin){				
				intersections.push_back(hlibre((*b)[j].ini,(*b)[j].fin ));
				j++;
			}else{
				intersections.push_back(hlibre((*b)[j].ini,(*a)[i].fin ));
				i++;
				if(i < a->size() && (*a)[i].ini< (*b)[j].fin){
					swap(a, b);swap(i, j);
				}else j++;
			}
		}else{//verificamos si b[j] no interseca con el siguiente a[i]
			i++;
			if(i<a->size() && (*a)[i].ini< (*b)[j].fin){
				swap(a, b);
				swap(i, j);
			}else j++;		
		}
	}
	return intersections;
}

int main(){
	int npeople, duration, h, m;

	vector<string> filesnames;
	string filename;
	cin>>npeople;
	hpeople **gente;
	gente = new hpeople*[npeople];
	for(int i=0; i< npeople; ++i){
		cin>>filename;
		gente[i] = new hpeople(filename);
		//gente[i]->p();
		//filesnames.push_back(filename);
	}
	scanf("%d:%d", &h, &m);
	duration=h2i(h, m);
	bool haydia;
	//cout<<duration<<endl;
	for(int i=0; i < WSIZE; ++i){
		haydia=true;
		for(int j=0; j < npeople; ++j){
			if (!gente[j]->free[i].size()){
				haydia=false; break;
			}
		}
		if(haydia){
			vector<hlibre> mixhoraslibre;
			for(int j=0; j < npeople; ++j){
				if (j==0){
					//copy(gente[j]->free[i], gfree[i]);
					gfree[i]=gente[j]->free[i];
				}else{
					gfree[i]=merge_free(&gfree[i], &gente[j]->free[i]);	
				}	
			}
		}
	}
	p(gfree, duration);
}
